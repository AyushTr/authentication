const express  = require('express')
const actions = require('../methods/actions')
const router = express.Router()

router.get('/', (req,res) =>{
    res.send('hello world')
})

router.post('/adduser', actions.addNew)
router.post('/authenticate', actions.authenticate)
module.exports = router